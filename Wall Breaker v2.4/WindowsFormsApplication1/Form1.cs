﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Media;
namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public int i, j, timp = 40, realizari = 0, gasit = 1;
        int t1 = 0, t2 = 0, t3 = 0;
        public int Level = 1;
        public PictureBox[,] wall = new PictureBox[50, 50];
        public int[,] a = new int[50, 50];
        public int[,] b = new int[50, 50];
        public int cx, cy, hx, hy, px, py;
        public int debloc_portal = 0, coins = 0, debloc_portal_bani = 0;
        public int cxx, cyy, t = 0, plansa = 1;
        public int scor = 0, viata = 3, scor_max = 0, highscore = 0, bani = 0, bani_cur = 0;
        public int bani_level1 = 0, bani_level2 = 0, bani_level3 = 0, bani_level4 = 0, bani_level5 = 0;
        public int bani_level6 = 0, bani_level7 = 0, bani_level8 = 0, bani_level9 = 0, bani_level10 = 0;
        public Image imgBrick = Image.FromFile("brick.png");
        public Image imgNegru = Image.FromFile("negru.jpg");
        public Image imgApa = Image.FromFile("water.png");
        public Image imgIarba = Image.FromFile("iarba.png");
        public Image imgViata = Image.FromFile("viata.jpg");
        public Image imgBomba = Image.FromFile("bombag.gif");
        public Image logo = Image.FromFile("logo.jpg");
        public Image game = Image.FromFile("game.png");
        public Image imgCasa = Image.FromFile("casa.png");
        public Image imgCufar = Image.FromFile("cufar.png");
        public Image imgBan = Image.FromFile("ban.png");
        public Image imgPortal = Image.FromFile("portal.png");
        public bool help = false;
        public string lPath = new string(' ',0);
        Random r = new Random();
        public int defWidth, defHeight;
        public int maxTime = 40, verif = 0, verif1 = 1, verif2 = 1, ver_ban = 0, ver_ban1 = 0;
        public int poz = 0, powerup = 0, deblocare = 0;
        public int moare = 0;                                          //realizarile 1,2,3
        public int lucky_one = 0, looper = 0, searcher = 0;            //realizarile 4,5,6
        public int fairman = 0;                                        //realizarea 7
        public string intro, game_over, win, life, big_prize, perfect, blink;
        public string humiliation, bomba;
        
        public Form1()
        {
            
            lPath = Environment.CurrentDirectory+"\\walls1.txt";
            intro = Environment.CurrentDirectory + "\\introduction.wav";
            win = Environment.CurrentDirectory + "\\crowd.wav";
            life = Environment.CurrentDirectory + "\\jump.wav";
            big_prize = Environment.CurrentDirectory + "\\talk_dirty.wav";
            perfect = Environment.CurrentDirectory + "\\perfect.wav";
            humiliation = Environment.CurrentDirectory + "\\humiliation.wav";
            bomba = Environment.CurrentDirectory + "\\bomba.wav";
            blink = Environment.CurrentDirectory + "\\blink.wav";
            var su = new System.Media.SoundPlayer(intro);
            su.Play();
            
            InitializeComponent();
            setMatrix(a);
            TextReader tr = new StreamReader(Application.StartupPath + "../res/high.txt");
            string scr = tr.ReadLine();
            tr.Close();
            highscore = Convert.ToInt32(scr);
            HighScoreStripStatusLabel.Text = "High score: " + highscore;
            for (i = 0; i < 15; i++)
                for (j = 0; j < 15; j++)
                {
                    wall[i, j] = new PictureBox();
                    wall[i, j].Name = "wall" + i.ToString() + j.ToString();
                    wall[i, j].Size = new Size(28, 28);
                    wall[i, j].Top = 25 + i * 28;
                    wall[i, j].Left = j * 28;
                    wall[i, j].Click += wall_Click;
                    if (a[i, j] == 0)                  //drum
                    {
                        wall[i, j].Image = imgNegru;
                    }
                    else
                        if (a[i, j] == 3)               //player
                        {
                            pictureBoxChicken.Location = new Point(wall[i, j].Left, wall[i, j].Top);
                            pictureBoxChicken.Image = imageList1.Images[poz];
                            wall[i, j].Image = imgNegru;
                            cx = i;
                            cy = j;
                            cxx = i;
                            cyy = j;
                            // pictureBoxChicken.BringToFront();
                        }
                        else
                            if (a[i, j] == 2)        //casa
                            {
                                pictureBoxCos.Image = imgCasa;
                                pictureBoxCos.Location = new Point(wall[i, j].Left, wall[i, j].Top);
                                hx = i;
                                hy = j;
                            }
                            else
                                if (a[i, j] == 1) //caramida
                                {
                                    wall[i, j].Image = imgBrick;
                                }
                                else
                                    if (a[i, j] == 4)      //iarba
                                    {
                                        wall[i, j].Image = imgIarba;
                                    }
                                    else
                                        if (a[i, j] == 5)  //apa
                                        {
                                            wall[i, j].Image = imgApa;
                                        }
                                        else
                                            if (a[i, j] == 6)   // viata
                                            {
                                                wall[i, j].Image = imgIarba;
                                            }
                                            else
                                                if (a[i, j] == 7)  //bomba
                                                {
                                                    wall[i, j].Image = imgIarba;
                                                }
                                                else
                                                    if (a[i, j] == 9)  //bani
                                                    {
                                                        wall[i, j].Image = imgBan;
                                                    }
                                                    else
                                                        if (a[i, j] == 10) //portal
                                                        {
                                                            wall[i, j].Image = imgIarba;
                                                        }
                                                        else if (a[i, j] == 11) //portal  intra coins
                                                        {
                                                            wall[i, j].Image = imgNegru;
                                                        }
                                                        else if (a[i, j] == 12) //portal iese coins
                                                        {
                                                            wall[i, j].Image = imgNegru;
                                                            px = i;
                                                            py = j;
                                                        }

                    this.Controls.Add(wall[i, j]);
                }
        }

        private void wall_Click(object sender, EventArgs e)
        {
           
            if (modificaLabirintToolStripMenuItem.Checked == false) return;

            if ((sender as PictureBox).Image == imgApa)
            {
                (sender as PictureBox).Image = imgIarba;
            }
            else
                if ((sender as PictureBox).Image == imgIarba)
                {
                    (sender as PictureBox).Image = imgBrick;
                }
                else
                    if ((sender as PictureBox).Image == imgBrick)
                    {
                        (sender as PictureBox).Image = imgNegru;
                    }
                    else
                        if ((sender as PictureBox).Image == imgNegru)
                        {
                            (sender as PictureBox).Image = imgApa;
                        }

            for (i = 0; i < 15 + t; i++)
                for (j = 0; j < 15 + t; j++)
                    if (wall[i, j].Image == imgNegru )
                        a[i, j] = 0;
                    else
                        if (wall[i, j].Image == imgBrick)
                            a[i, j] = 1;
                        else
                            if (wall[i, j].Image == imgCasa)
                                a[i, j] = 2;
                            else
                                if (wall[i, j].Image == imgIarba)
                                    a[i, j] = 4;
                                else
                                    if (wall[i, j].Image == imgApa)
                                        a[i, j] = 5;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (viata <= 0) return;
            if (modificaLabirintToolStripMenuItem.Checked == true) return;
            if (e.KeyData == Keys.Down)
            {
                poz++;
                if (poz > 2)
                    poz = 0;
                if (cx + 1 < 15 + t && (a[cx + 1, cy] == 0 || a[cx + 1, cy] == 2 || a[cx + 1, cy] == 3 || a[cx + 1, cy] == 4 || a[cx + 1, cy] == 6 || a[cx + 1, cy] == 7 || a[cx + 1, cy] == 8 || a[cx + 1, cy] == 9 || a[cx + 1, cy] == 10 || a[cx + 1, cy] == 11 || a[cx + 1, cy] == 12))
                {
                    pictureBoxChicken.Image = imageList1.Images[poz];
                    cx++;
                    pictureBoxChicken.Top += 28;
                }
            }
            else
                if (e.KeyData == Keys.Up)
                {
                    poz = 9;
                    poz++;
                    if (poz > 11)
                        poz = 9;
                    if (cx - 1 >= 0 && (a[cx - 1, cy] == 0 || a[cx - 1, cy] == 2 || a[cx - 1, cy] == 3 || a[cx - 1, cy] == 4 || a[cx - 1, cy] == 6 || a[cx - 1, cy] == 7 || a[cx - 1, cy] == 8 || a[cx - 1, cy] == 9 || a[cx - 1, cy] == 10 || a[cx - 1, cy] == 11 || a[cx - 1, cy] == 12))
                    {
                        pictureBoxChicken.Image = imageList1.Images[poz];
                        cx--;
                        pictureBoxChicken.Top -= 28;
                    }
                }
                else
                    if (e.KeyData == Keys.Left)
                    {
                        poz = 3;
                        poz++;
                        if (poz > 5)
                            poz = 3;
                        if (cy - 1 >= 0 && (a[cx, cy - 1] == 0 || a[cx, cy - 1] == 2 || a[cx, cy - 1] == 3 || a[cx, cy - 1] == 4 || a[cx, cy - 1] == 6 || a[cx, cy - 1] == 7 || a[cx, cy - 1] == 8 || a[cx, cy - 1] == 9 || a[cx, cy - 1] == 10 || a[cx, cy - 1] == 11 || a[cx, cy - 1] == 12))
                        {
                            pictureBoxChicken.Image = imageList1.Images[poz];
                            cy--;
                            pictureBoxChicken.Left -= 28;
                        }
                    }
                    else
                        if (e.KeyData == Keys.Right)
                        {
                            poz = 6;
                            poz++;
                            if (poz > 8)
                                poz = 6;
                            if (cy + 1 < 15 + t && (a[cx, cy + 1] == 0 || a[cx, cy + 1] == 2 || a[cx, cy + 1] == 3 || a[cx, cy + 1] == 4 || a[cx, cy + 1] == 6 || a[cx, cy + 1] == 7 || a[cx, cy + 1] == 8 || a[cx, cy + 1] == 9 || a[cx, cy + 1] == 10 || a[cx, cy + 1] == 11 || a[cx, cy + 1] == 12))
                            {
                                pictureBoxChicken.Image = imageList1.Images[poz];
                                cy++;
                                pictureBoxChicken.Left += 28;
                            }
                        }

            if (a[cx, cy] == 4 || a[cx, cy] == 6 || a[cx, cy] == 7 || a[cx, cy] == 0 || a[cx, cy] == 3 || a[cx, cy] == 10 || a[cx, cy] == 11 || a[cx, cy] == 12)
                pictureBoxChicken.BackgroundImage = wall[cx, cy].Image;
            
            

            if (a[cx, cy] == 6)           //viata
            {
                searcher++;
                var su = new System.Media.SoundPlayer(life);
                su.Play();
                viata++;
                scor += 10;
                timer.Enabled = false;
                MessageBox.Show("Ai gasit viata secreta!");
                timer.Enabled = true;
                a[cx, cy] = 0;
                toolStripStatusViata.Text = "Viata: " + viata.ToString();
                toolStripStatusScor.Text = "Scor: " + scor.ToString();
            }

            if (a[cx, cy] == 7)         //bomba
            {
                moare++;
                if (plansa == 1 || plansa == 2)
                {
                    var su1 = new System.Media.SoundPlayer(humiliation);
                    su1.Play();
                    bani = 0;
                    scor -= bani_cur * 25;
                }
                else
                    if (plansa == 3)
                    {
                        var su3 = new System.Media.SoundPlayer(humiliation);
                        su3.Play();
                        bani -= (bani_level2 + bani_cur);
                        scor -= (bani_level2 + bani_cur) * 25;
                    }
                    else
                        if (plansa == 4)
                        {
                            var su4 = new System.Media.SoundPlayer(bomba);
                            su4.Play();
                            bani -= (bani_level3 + bani_cur);
                            scor -= (bani_level3 + bani_cur) * 25;
                        }
                        else
                            if (plansa == 5)
                            {
                                var su5 = new System.Media.SoundPlayer(bomba);
                                su5.Play();
                                bani -= (bani_level4 + bani_cur);
                                scor -= (bani_level4 + bani_cur) * 25;
                            }
                            else
                                if (plansa == 6)
                                {
                                    var su6 = new System.Media.SoundPlayer(bomba);
                                    su6.Play();
                                    bani -= (bani_level5 + bani_cur);
                                    scor -= (bani_level5 + bani_cur) * 25;
                                }
                                else
                                    if (plansa == 7)
                                    {
                                        var su7 = new System.Media.SoundPlayer(bomba);
                                        su7.Play();
                                        bani -= (bani_level6 + bani_cur);
                                        scor -= (bani_level6 + bani_cur) * 25;
                                    }
                                    else
                                        if (plansa == 8)
                                        {
                                            var su8 = new System.Media.SoundPlayer(bomba);
                                            su8.Play();
                                            bani -= (bani_level7 + bani_cur);
                                            scor -= (bani_level7 + bani_cur) * 25;
                                        }
                                        else if (plansa == 9)
                                        {
                                            var su9 = new System.Media.SoundPlayer(bomba);
                                            su9.Play();
                                            bani -= (bani_level8 + bani_cur);
                                            scor -= (bani_level8 + bani_cur) * 25;
                                        }
                timer.Enabled = false;
                viata--;
                scor -= 33;
                bani_cur = 0;
                baniStripStatusLabel.Text = "Bani: " + bani.ToString() + "/20";

                if (viata == 0)
                {
                    bani = 0;
                    baniStripStatusLabel.Text = "Bani: " + bani.ToString() + "/20";
                    toolStripStatusViata.Text = "Viata: " + viata.ToString();
                    toolStripStatusLabelTime.Text = "Timp: 0 sec";
                    pictureBox1.Visible = true;
                    pictureBox1.Image = game;
                    pictureBoxChicken.Visible = false;
                    modificaLabirintToolStripMenuItem.Enabled = false;
                    salveazaLabirintToolStripMenuItem.Enabled = false;
                    incarcaLabirintToolStripMenuItem.Enabled = false;
                    MessageBox.Show("Ati nimerit intr-o bomba!");
                    plansa = 1;
                    lPath = "walls" + plansa.ToString() + ".txt";
                    toolStripStatusScor.Text = "Scor: " + scor.ToString();
                    if (scor > highscore)
                    {
                        highscore = scor;
                        HighScoreStripStatusLabel.Text = "High score: " + highscore;
                        MessageBox.Show("Ai obtinut un nou high score!");
                        TextWriter tw = new StreamWriter(Application.StartupPath + "../res/high.txt");
                        tw.Write(highscore);
                        tw.Close();
                    }
                }
                else
                {
                    toolStripStatusViata.Text = "Viata: " + viata.ToString();
                    toolStripStatusLabelTime.Text = "Timp: " + timp.ToString() + " sec";
                    toolStripStatusScor.Text = "Scor: " + scor.ToString();
                    MessageBox.Show("Ati nimerit intr-o bomba!");
                    if (plansa > 1)
                    {
                        plansa--;
                        lPath = "walls" + plansa.ToString() + ".txt";
                        label2.Text = "Level " + plansa.ToString();
                    }
                    jocNouToolStripMenuItem_Click(sender, e);
                }
            }

            if (bani == 20 && verif1 == 1)  //intrebare deblocare portal coins
            {
                if (MessageBox.Show("Doriti sa deblocati portalul spre lumea banilor?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    bani -= 20;
                    baniStripStatusLabel.Text = "Bani: " + bani.ToString() + "/150";
                }
                else
                {
                    debloc_portal_bani = 0;
                }
                verif1 = 0;
                ver_ban = 1;
            }

            if (bani == 150 && verif2 == 1)  //intrebare deblocare cufar
            {
                if (MessageBox.Show("Doriti sa deblocati cufarul magic pentru 150 de bani?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    bani -= 150;
                    baniStripStatusLabel.Text = "Bani: " + bani.ToString() + "/150";
                    deblocare = 1;
                }
                else
                {
                    deblocare = 0;
                }
                verif2 = 0;
                ver_ban1 = 1;
            }


            if (a[cx, cy] == 8 && deblocare == 1)   //cufar
            {
                powerup = r.Next(1, 3);
                if (powerup == 1)         //lumina
                {
                    lucky_one = 1;
                    MessageBox.Show("Ati dobandit puterea de a vedea bombele!");
                    for (int i = 0; i < 14 + t; i++)
                        for (int j = 0; j < 14 + t; j++)
                            if (a[i, j] == 7)
                            {
                                wall[i, j].BackgroundImage = imgIarba;
                                wall[i, j].Image = imgBomba;
                            }
                    a[cx, cy] = 4;
                    wall[cx, cy].Image = imgIarba;
                }
                else
                    if (powerup == 2)    //portal
                    {
                        debloc_portal = 1;
                        MessageBox.Show("Ati deblocat un portal!");
                        for (int i = 0; i < 14 + t; i++)
                            for (int j = 0; j < 14 + t; j++)
                                if (a[i, j] == 10)
                                {
                                    wall[i, j].BackgroundImage = imgIarba;
                                    wall[i, j].Image = imgPortal;
                                }
                        a[cx, cy] = 4;
                        wall[cx, cy].Image = imgIarba;
                    }
                deblocare = 0;
            }

            if (a[cx, cy] == 10 && debloc_portal == 1)  //intra in portal
            {
                looper = 1;
                a[cx, cy] = 4;
                wall[cx, cy].Image = imgIarba;
                for (int i = 0; i < 14 + t; i++)
                    for (int j = 0; j < 14 + t; j++)
                        if (a[i, j] == 10)
                        {
                            cx = i;
                            cy = j;
                            a[cx, cy] = 4;
                            wall[cx, cy].Image = imgIarba;
                        }
                pictureBoxChicken.Location=new Point(wall[cx, cy].Left, wall[cx, cy].Top);
                debloc_portal = 0;
            }

            if (a[cx, cy] == 9)       //bani
            {
                var su = new System.Media.SoundPlayer(blink);
                su.Play();
                bani++;    //bani totali
                bani_cur++; //calculeaza pe level
                scor += 25;
                wall[cx, cy].Image = imgNegru;
                pictureBoxChicken.BackgroundImage = imgNegru;
                a[cx, cy] = 0;
                if(ver_ban==0)
                baniStripStatusLabel.Text = "Bani: " + bani.ToString() + "/20";
                else
                    baniStripStatusLabel.Text = "Bani: " + bani.ToString() + "/150";
                toolStripStatusScor.Text = "Scor: " + scor.ToString();
            }

            if (a[cx, cy] == 11 && gasit==1) //plansa coins
            {
                gasit = 0;
                lPath = "wall_coins.txt";
                setMatrix(a);
                coins = 1;
                jocNouToolStripMenuItem_Click(sender, e);   
            }

            if (a[cx, cy] == 12) //plansa intoarce coins
            {
                lPath = "walls" + plansa.ToString() + ".txt";
                setMatrix(a);
                jocNouToolStripMenuItem_Click(sender, e);
            }


            if (cx == hx && cy == hy) //ajunde la destinatie
            {
                if (scor >= 500 && t1 == 0)   //realizarea 10
                {
                    MessageBox.Show("Ati deblocat ultima realizare");
                    achievement10.Load("Medal_bronze.png");
                    realizari++;
                    titluachievements.Text = "Realizari deblocate: " + realizari.ToString() + "/10";
                    t1 = 1;
                }

                if (scor >= 1000 && t2 == 0)   //realizarea 9
                {
                    MessageBox.Show("Ati deblocat realizarea 9");
                    achievement9.Load("Medal_silver.png");
                    realizari++;
                    titluachievements.Text = "Realizari deblocate: " + realizari.ToString() + "/10";
                    t2 = 1;
                }

                if (scor >= 1500 && t3 == 0)   //realizarea 8
                {
                    MessageBox.Show("Ati deblocat realizarea 8");
                    achievement8.Load("Medal_gold.png");
                    realizari++;
                    titluachievements.Text = "Realizari deblocate: " + realizari.ToString() + "/10";
                    t3 = 1;
                }

                timer.Enabled = false;
                if (plansa == 1)
                {
                    var su1 = new System.Media.SoundPlayer(win);
                    su1.Play();
                    bani_level1 = bani_cur;
                }
                if (plansa == 2)
                {
                    var su2 = new System.Media.SoundPlayer(win);
                    su2.Play();
                    bani_level2 = bani_cur;
                }
                if (plansa == 3)
                {
                    var su3 = new System.Media.SoundPlayer(perfect);
                    su3.Play();
                    bani_level3 = bani_cur;
                }
                if (plansa == 4)
                {
                    var su4 = new System.Media.SoundPlayer(perfect);
                    su4.Play();
                    bani_level4 = bani_cur;
                }
                if (plansa == 5)
                {
                    var su5 = new System.Media.SoundPlayer(perfect);
                    su5.Play();
                    bani_level5 = bani_cur;
                }
                if (plansa == 6)
                {
                    var su6 = new System.Media.SoundPlayer(perfect);
                    su6.Play();
                    bani_level6 = bani_cur;
                }
                if (plansa == 7)
                {
                    var su7 = new System.Media.SoundPlayer(perfect);
                    su7.Play();
                    bani_level7 = bani_cur;
                }
                if (plansa == 8)
                {
                    var su8 = new System.Media.SoundPlayer(perfect);
                    su8.Play();
                    bani_level8 = bani_cur;
                }
                if (plansa == 9)
                {
                    var su9 = new System.Media.SoundPlayer(perfect);
                    su9.Play();
                    bani_level9 = bani_cur;
                }
                plansa++;
                MessageBox.Show("Ati castigat!!!");
                if (plansa <= 10)
                {
                    if (plansa == 4)
                    {
                        maxTime = 60;
                    }
                    if (plansa == 5)
                    {
                        maxTime = 65;
                    }
                    if (plansa == 6)
                    {
                        maxTime = 70;
                    }
                    if (plansa == 7)
                    {
                        maxTime = 75;
                    }
                    if (plansa == 8)
                    {
                        maxTime = 80;
                    }
                    if (plansa == 9)
                    {
                        maxTime = 85;
                    }
                    lPath = "walls" + plansa.ToString() + ".txt";
                    label2.Text = "Level " + plansa.ToString();
                    cx = cxx; cy = cyy;
                    pictureBoxChicken.Location = new Point(wall[cx, cy].Left, wall[cx, cy].Top);
                    pictureBoxChicken.BringToFront();
                    scor += 100;
                    timp = maxTime;
                    toolStripStatusScor.Text = "Scor: " + scor;
                    timer.Enabled = true;
                    jocNouToolStripMenuItem_Click(sender, e);
                }
                else
                {
                    maxTime = 90;
                    jocNouToolStripMenuItem.Enabled = false;
                    var su1 = new System.Media.SoundPlayer(big_prize);
                    su1.Play();
                    pictureBox1.Visible = true;
                    pictureBox1.Image = logo;
                    timer.Enabled = false;
                    pictureBoxChicken.Visible = false;
                    pictureBoxCos.Visible = false;
                    modificaLabirintToolStripMenuItem.Enabled = false;
                    salveazaLabirintToolStripMenuItem.Enabled = false;
                    incarcaLabirintToolStripMenuItem.Enabled = false;
                    if (scor > highscore)
                    {
                        highscore = scor;
                        HighScoreStripStatusLabel.Text = "High score: " + highscore;
                        MessageBox.Show("Ai obtinut un nou high score!");
                        TextWriter tw = new StreamWriter(Application.StartupPath + "../res/high.txt");
                        tw.Write(highscore);
                        tw.Close();
                    }
                    plansa = 1;
                    scor = 0;
                    timp = 20;
                    viata = 3;
                    maxTime = 20;

                    if (moare == 0)   //realizarea 1
                    {
                        realizari++;
                        titluachievements.Text = "Realizari deblocate: " + realizari.ToString() + "/10";
                        MessageBox.Show("Ati deblocat prima realizare!");
                        achievement1.Load("Trophy_Gold.png");
                    }

                    if (moare == 1)   //realizarea 2
                    {
                        realizari++;
                        titluachievements.Text = "Realizari deblocate: " + realizari.ToString() + "/10";
                        MessageBox.Show("Ati deblocat realizarea 2!");
                        achievement2.Load("Trophy_Silver.png");
                    }

                    if (moare == 2)   //realizarea 3
                    {
                        realizari++;
                        titluachievements.Text = "Realizari deblocate: " + realizari.ToString() + "/10";
                        MessageBox.Show("Ati deblocat realizarea 3!");
                        achievement3.Load("Trophy_Bronze.png");
                    }

                    if (lucky_one == 0)   //realizarea 4
                    {
                        realizari++;
                        titluachievements.Text = "Realizari deblocate: " + realizari.ToString() + "/10";
                        MessageBox.Show("Ati deblocat realizarea 4!");
                        achievement4.Load("lucky.png");
                    }

                    if (looper == 0)   //realizarea 5
                    {
                        realizari++;
                        titluachievements.Text = "Realizari deblocate: " + realizari.ToString() + "/10";
                        MessageBox.Show("Ati deblocat realizarea 5!");
                        achievement5.Load("portal.png");
                    }

                    if (searcher==12)   //realizarea 6
                    {
                        realizari++;
                        titluachievements.Text = "Realizari deblocate: " + realizari.ToString() + "/10";
                        MessageBox.Show("Ati deblocat realizarea 6!");
                        achievement6.Load("searcher.gif");
                    }

                    if (fairman == 0)  //realizarea 7
                    {
                        realizari++;
                        titluachievements.Text = "Realizari deblocate: " + realizari.ToString() + "/10";
                        MessageBox.Show("Ati deblocat realizarea 7!");
                        achievement7.Load("change.png");
                    }                  
                }
            }
        }

        private void iesireToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }//iesire joc

        public void Fill(int x, int y)
        {
            if (x >= 0 && x <= 14 + t && y >= 0 && y <= 14 + t && (a[x, y] == 0 || a[x, y] == 2 || 
                a[x, y] == 3 || a[x, y] == 4 || a[x, y] == 6 || a[x, y] == 8 || a[x, y] == 9 || 
                a[x,y]==10 || a[x,y]==11 || a[x,y]==12) && b[x, y] == 0)
            {
                b[x, y] = 1;
                Fill(x, y + 1);
                Fill(x, y - 1);
                Fill(x - 1, y);
                Fill(x + 1, y);
            }
        }//matricea drumurilor

        private void modificaLabirintToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fairman = 1; //jucatorul a folosit functia de modificare labirint
            if (modificaLabirintToolStripMenuItem.Checked == true)
            {
                for (i = 0; i < 15 + t; i++)
                    for (j = 0; j < 15 + t; j++)
                        b[i, j] = 0;
                Fill(cx, cy);
                if (b[hx, hy] == 0)
                {
                    MessageBox.Show("Labirintul nu poate fi rezolvat");
                    return;
                }
                timer.Enabled = true;              
                modificaLabirintToolStripMenuItem.Checked = false;
                this.Focus();
            }
            else
            {
                timer.Enabled = false;               
                modificaLabirintToolStripMenuItem.Checked = true;
                for (i = 0; i < 15 + t; i++)
                    for (j = 0; j < 15 + t; j++)
                        b[i, j] = 0;
            }
        }// modifica formul

        private void salveazaLabirintToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer.Enabled = false;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                StreamWriter sw = new StreamWriter(saveFileDialog.FileName);
                for (i = 0; i < 15 + t; i++)
                    for (j = 0; j < 15 + t; j++)
                        sw.Write(a[i, j].ToString() + (j==15+t-1?(i<15+t-1?Environment.NewLine:""):" "));
                sw.Flush();
                sw.Dispose();
            }
            timer.Enabled = true;
        }// salveaza labirintul

        private void incarcaLabirintToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer.Enabled = false;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                timp = maxTime;
                lPath = openFileDialog.FileName;
                setMatrix(a);
                verif = 1;
                jocNouToolStripMenuItem_Click(sender, e);
                
            }
            timer.Enabled = true;
        }//incarca un nou labirint

        private void jocNouToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bani_cur = 0; //initializeaza banii pentru fiecare plansa
            if (viata == 0)
            {
                viata = 3;
                scor = 0;
                toolStripStatusViata.Text = "Viata: " + viata.ToString(); 
                toolStripStatusScor.Text = "Scor: " + scor.ToString();
                lPath = "walls1.txt";
            }
            if (verif == 0 && coins == 0)  //verifica daca a fost o incarcata o plansa
            {
                lPath = "walls" + plansa.ToString() + ".txt";
                setMatrix(a);
            }
            verif = 0;
            coins = 0;
            pictureBox1.Visible = false;
            modificaLabirintToolStripMenuItem.Enabled = true;
            salveazaLabirintToolStripMenuItem.Enabled = true;
            incarcaLabirintToolStripMenuItem.Enabled = true;
            t = 0;
            this.Width = defWidth;                        
            this.Height = defHeight;
            toolStripStatusLabelTime.Text = "Timp: "+maxTime.ToString()+" sec";
            toolStripStatusScor.Text = "Scor: " + scor.ToString();
            toolStripStatusViata.Text = "Viata:" + viata.ToString();
            this.Refresh();
                    
            pictureBoxChicken.Location = new Point(wall[cx, cy].Left, wall[cx, cy].Top);
            pictureBoxChicken.Image = imageList1.Images[0];
            pictureBoxChicken.BackgroundImage = imgNegru;
            for (i = 0; i < 15; i++)
                for (j = 0; j < 15; j++)
                {
                    if (a[i, j] == 0)  //drum
                    {
                        wall[i, j].Image = imgNegru;
                    }
                    else
                        if (a[i, j] == 2) //casa
                        {
                            pictureBoxCos.Location = new Point(wall[i, j].Left, wall[i, j].Top);
                            hx = i;
                            hy = j;
                            wall[i, j].Image = imgCasa;
                        }
                        else
                            if (a[i, j] == 1) //zid
                            {
                                wall[i, j].Image = imgBrick;
                            }
                            else
                                if (a[i, j] == 3) //player
                                {
                                    cxx = i;
                                    cyy = j;
                                    cx = cxx;
                                    cy = cyy;
                                    pictureBoxChicken.Location = new Point(wall[i, j].Left, wall[i, j].Top);
                                    wall[i, j].Image = imgNegru;
                                }
                                else
                                    if (a[i, j] == 4)  //iarba
                                    {
                                        wall[i, j].Image = imgIarba;
                                    }
                                    else
                                        if (a[i, j] == 5)   //apa
                                        {
                                            wall[i, j].Image = imgApa;
                                        }
                                        else
                                            if (a[i, j] == 6)  //viata
                                            {
                                                wall[i, j].Image = imgIarba;
                                            }
                                            else
                                                if (a[i, j] == 7)  //bomba
                                                {
                                                    wall[i, j].Image = imgIarba;
                                                }
                                                else
                                                    if (a[i, j] == 8 && deblocare == 1) //cufar deblocat
                                                    {
                                                        wall[i, j].BackgroundImage = imgIarba;
                                                        wall[i, j].Image = imgCufar;
                                                    }
                                                    else
                                                        if (a[i, j] == 9)  //bani
                                                        {
                                                            wall[i, j].Image = imgBan;
                                                        }
                                                        else
                                                            if (a[i, j] == 8 && deblocare == 0) //cufar nedeblocat
                                                            {
                                                                wall[i, j].BackgroundImage = imgIarba;
                                                                wall[i, j].Image = imgIarba;
                                                            }
                                                            else
                                                                if (a[i, j] == 10 && debloc_portal == 0) //portal
                                                                {
                                                                   // wall[i, j].BackgroundImage = img;
                                                                    wall[i, j].Image = imgNegru;
                                                                }
                                                                else
                                                                    if (a[i, j] == 10 && debloc_portal == 1) //portal deblocat
                                                                    {
                                                                        wall[i, j].Image = imgPortal;
                                                                    }
                                                                    else
                                                                        if (a[i, j] == 11 && debloc_portal_bani == 1) //portal coins deblocat
                                                                        {
                                                                            wall[i, j].Image = imgPortal;
                                                                        }
                                                                        else
                                                                            if (a[i, j] == 11 && debloc_portal_bani == 0) //portal coins nedeblocat
                                                                            {
                                                                                wall[i, j].BackgroundImage = imgNegru;
                                                                            }
                                                                            else
                                                                                if (a[i, j] == 12) //portal intoarce
                                                                                {
                                                                                    wall[i, j].BackgroundImage = imgIarba;
                                                                                    wall[i, j].Image = imgPortal;
                                                                                }
                }
            
            this.Refresh();
            timp = maxTime;
            timer.Enabled = true;
            pictureBoxChicken.Visible = true;
            pictureBoxCos.Visible = true;
        }//parcurgerea jocului
       
        private void ajutorToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
            if (help == false)
            {
                help = true;
                richTextBox1.Visible = true;
                richTextBox1.LoadFile("labirint.rtf");
                timer.Enabled = false;
            }
            else
            {
                help = false;
                richTextBox1.Visible = false;
                timer.Enabled = true ;
                this.Focus();
            }
        } //meniul ajutor

        private void timer1_Tick(object sender, EventArgs e)
        {
            timp--;
            toolStripStatusLabelTime.Text = "Timp: ";
            if (timp % 60 < maxTime)               
                toolStripStatusLabelTime.Text += timp % 80 + " sec";
            else
                toolStripStatusLabelTime.Text += timp % 80 + " sec";
            if (timp == 0)
            {
                timer.Enabled = false;
                MessageBox.Show("A expirat timpul.");
                viata--;
                moare++;
                scor -= 33;
                if (viata == 0)
                {
                    MessageBox.Show("Ati pierdut jocul!");
                    pictureBox1.Visible = true;
                    pictureBoxChicken.Visible = false;
                    modificaLabirintToolStripMenuItem.Enabled = false;
                    salveazaLabirintToolStripMenuItem.Enabled = false;
                    incarcaLabirintToolStripMenuItem.Enabled = false;
                }
                else
                    jocNouToolStripMenuItem_Click(sender, e);
                toolStripStatusViata.Text = "Viata: " + viata;
                toolStripStatusScor.Text = "Scor: " + scor;   
            }
        } //timer pentru fiecare plansa

        private void Form1_Load(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            label1.Text = "";
            timer.Enabled = false;
            timer1.Interval = 70;
            toolStripProgressBarLoading.Maximum = 100;
            timer1.Tick += new EventHandler(timer1_Tick_1);
            pictureBox1.Visible = true;
            pictureBox1.Image = logo;
            pictureBoxChicken.Visible = false;
            pictureBoxCos.Visible = false;
            toolStripProgressBarLoading.Visible = true;
            defWidth = this.Width;              
            defHeight = this.Height;           
        } //initializarea jocului

        private void maresteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            t++;
            label2.Top += 28 * t;
            this.Size = new Size(defWidth + t * 28, defHeight + t * 28);                                                              
            for (i = 0; i < 15 + t; i++)                       
            {                                                    
                a[14 + t, i] =0;             
                wall[14 + t, i] = new PictureBox();
                wall[14 + t, i].Name = "wall" + i.ToString() + j.ToString();
                wall[14 + t, i].Image = imgNegru;
                wall[14 + t, i].Size = new Size(28, 28);
                wall[14 + t, i].Top = menuStrip.Height + 1 + (14 + t) * 28;         
                wall[14 + t, i].Left = i * 28;
                wall[14 + t, i].Click += wall_Click;
                this.Controls.Add(wall[14 + t, i]);
            }

            for (i = 0; i < 15 + t; i++)
            {
                a[i, 14 + t] = 0;
                wall[i, 14 + t] = new PictureBox();
                wall[i, 14 + t].Name = "wall" + i.ToString() + j.ToString();
                wall[i, 14 + t].Image = imgNegru;
                wall[i, 14 + t].Size = new Size(28, 28);
                wall[i, 14 + t].Top = menuStrip.Height + 1 + i * 28;      
                wall[i, 14 + t].Left = (14 + t) * 28;
                wall[i, 14 + t].Click += wall_Click;
                this.Controls.Add(wall[i, 14 + t]);
            }
        }  //creste marimea Form-ului

        private void micsoreazaToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (hx == 14 + t || hy ==14+t || cx==14+t || cy==14+t || cxx==14+t || cyy==14+t)
                return;
            for (i = 0; i < 15 + t; i++) //coloana          
                a[14 + t, i] = 0;

            for (i = 0; i < 15 + t; i++) //linie
                a[i, 14 + t] = 0;

            for (i = 0; i < 15 + t; i++)
                this.Controls.Remove(wall[i, 14 + t]);

            for (i = 0; i < 15 + t; i++)
                this.Controls.Remove(wall[14 + t, i]);
            t--;
            label2.Top -= 28 * t;
            this.Size = new Size(defWidth + t * 28, defHeight + t * 28);
        } //scade marimea Form-ului

        private void setMatrix(int[,] a)
        {                                                   
            string fileText = File.ReadAllText(lPath);
            string[] lFileText = fileText.Split('\n');
            string[] cFileText = new string[lFileText.Length];
            for (int i = 0; i < lFileText.Length; i++)
            {
                
                cFileText = lFileText[i].Split(' ');
                for (int j = 0; j < cFileText.Length; j++)
                {
                    a[i, j] = int.Parse(cFileText[j]);
                }
            }
        }  //citeste matricea din fisier

        private void timer1_Tick_1(object sender, EventArgs e) //timer pentru progress bar
        {
            if (toolStripProgressBarLoading.Value != 100)
            {
                toolStripProgressBarLoading.Value++;
            }
            else
            {
                achivementsToolStripMenuItem.Enabled = true;
                ajutorToolStripMenuItem.Enabled = true;
                timer1.Enabled=false;
                pictureBox1.Visible = false;
                toolStripProgressBarLoading.Visible = false;
                baniStripStatusLabel.Visible = true;
                pictureBoxChicken.Visible = true;
                pictureBoxCos.Visible = true;
                timer.Enabled = true;
                label2.Visible = true;
            }
        }

        private void achivementsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (help == false)
            {
                help = true;
                panel.BringToFront();
                panel.Visible = true;
                timer.Enabled = false;
            }
            else
            {
                help = false;
                panel.Visible = false;
                timer.Enabled = true;
                this.Focus();
            }
        }//meniul de realizari

        private void achievement1_Click(object sender, EventArgs e)
        {
            label3.Text = "Zeus";
            label4.Text = "Termina jocul fara sa intri in bombe"; 
        }

        private void achievements2_Click(object sender, EventArgs e)
        {
            label3.Text = "Hero";
            label4.Text = "Termina jocul murind o singura data";
        }

        private void achievement3_Click(object sender, EventArgs e)
        {
            label3.Text = "Survivor";
            label4.Text = "Termina jocul murind doar de 2 ori";
        }

        private void achievement4_Click(object sender, EventArgs e)
        {
            label3.Text = "Lucky";
            label4.Text = "Termina jocul fara sa vezi bombele";
        }

        private void achievement5_Click(object sender, EventArgs e)
        {
            label3.Text = "Looper";
            label4.Text = "Termina jocul fara sa folosesti portalul";
        }

        private void achievement6_Click(object sender, EventArgs e)
        {
            label3.Text = "Searcher";
            label4.Text = "Gaseste toate vietile secrete";
        }

        private void achievement7_Click(object sender, EventArgs e)
        {
            label3.Text = "Fairman";
            label4.Text = "Termina jocul fara sa modifici plansile";
        }

        private void achievement8_Click(object sender, EventArgs e)
        {
            label3.Text = "Gold";
            label4.Text = "Scor de 1500 de puncte";
        }

        private void achievement9_Click(object sender, EventArgs e)
        {
            label3.Text = "Silver";
            label4.Text = "Scor de 1000 de puncte";
        }

        private void achievement10_Click(object sender, EventArgs e)
        {
            label3.Text = "Bronz";
            label4.Text = "Scor de 500 de puncte";
        }

        private void reseteazaJocToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }
    }
}
