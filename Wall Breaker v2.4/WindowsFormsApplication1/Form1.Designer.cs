﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.jocToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jocNouToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificaLabirintToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.micsoreazaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.maresteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.salveazaLabirintToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.incarcaLabirintToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reseteazaJocToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.iesireToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajutorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.achivementsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBoxCos = new System.Windows.Forms.PictureBox();
            this.pictureBoxChicken = new System.Windows.Forms.PictureBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusViata = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusScor = new System.Windows.Forms.ToolStripStatusLabel();
            this.HighScoreStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBarLoading = new System.Windows.Forms.ToolStripProgressBar();
            this.baniStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panel = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.achievement10 = new System.Windows.Forms.PictureBox();
            this.achievement9 = new System.Windows.Forms.PictureBox();
            this.achievement8 = new System.Windows.Forms.PictureBox();
            this.achievement7 = new System.Windows.Forms.PictureBox();
            this.achievement6 = new System.Windows.Forms.PictureBox();
            this.achievement5 = new System.Windows.Forms.PictureBox();
            this.achievement4 = new System.Windows.Forms.PictureBox();
            this.achievement3 = new System.Windows.Forms.PictureBox();
            this.achievement2 = new System.Windows.Forms.PictureBox();
            this.achievement1 = new System.Windows.Forms.PictureBox();
            this.titluachievements = new System.Windows.Forms.Label();
            this.menuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxChicken)).BeginInit();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.achievement10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.achievement9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.achievement8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.achievement7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.achievement6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.achievement5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.achievement4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.achievement3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.achievement2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.achievement1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.menuStrip.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.jocToolStripMenuItem,
            this.ajutorToolStripMenuItem,
            this.achivementsToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(420, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // jocToolStripMenuItem
            // 
            this.jocToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.jocToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.jocNouToolStripMenuItem,
            this.modificaLabirintToolStripMenuItem,
            this.salveazaLabirintToolStripMenuItem,
            this.incarcaLabirintToolStripMenuItem,
            this.reseteazaJocToolStripMenuItem,
            this.toolStripSeparator1,
            this.iesireToolStripMenuItem});
            this.jocToolStripMenuItem.Name = "jocToolStripMenuItem";
            this.jocToolStripMenuItem.Size = new System.Drawing.Size(36, 20);
            this.jocToolStripMenuItem.Text = "Joc";
            // 
            // jocNouToolStripMenuItem
            // 
            this.jocNouToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.jocNouToolStripMenuItem.Name = "jocNouToolStripMenuItem";
            this.jocNouToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.jocNouToolStripMenuItem.Text = "Joc Nou";
            this.jocNouToolStripMenuItem.Click += new System.EventHandler(this.jocNouToolStripMenuItem_Click);
            // 
            // modificaLabirintToolStripMenuItem
            // 
            this.modificaLabirintToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.modificaLabirintToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.micsoreazaToolStripMenuItem,
            this.maresteToolStripMenuItem1});
            this.modificaLabirintToolStripMenuItem.Name = "modificaLabirintToolStripMenuItem";
            this.modificaLabirintToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.modificaLabirintToolStripMenuItem.Text = "Modifica Labirint";
            this.modificaLabirintToolStripMenuItem.Click += new System.EventHandler(this.modificaLabirintToolStripMenuItem_Click);
            // 
            // micsoreazaToolStripMenuItem
            // 
            this.micsoreazaToolStripMenuItem.BackColor = System.Drawing.Color.Green;
            this.micsoreazaToolStripMenuItem.Name = "micsoreazaToolStripMenuItem";
            this.micsoreazaToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.micsoreazaToolStripMenuItem.Text = "Micsoreaza";
            this.micsoreazaToolStripMenuItem.Click += new System.EventHandler(this.micsoreazaToolStripMenuItem_Click_1);
            // 
            // maresteToolStripMenuItem1
            // 
            this.maresteToolStripMenuItem1.BackColor = System.Drawing.Color.Green;
            this.maresteToolStripMenuItem1.Name = "maresteToolStripMenuItem1";
            this.maresteToolStripMenuItem1.Size = new System.Drawing.Size(133, 22);
            this.maresteToolStripMenuItem1.Text = "Mareste";
            this.maresteToolStripMenuItem1.Click += new System.EventHandler(this.maresteToolStripMenuItem1_Click);
            // 
            // salveazaLabirintToolStripMenuItem
            // 
            this.salveazaLabirintToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.salveazaLabirintToolStripMenuItem.Name = "salveazaLabirintToolStripMenuItem";
            this.salveazaLabirintToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.salveazaLabirintToolStripMenuItem.Text = "Salveaza labirint";
            this.salveazaLabirintToolStripMenuItem.Click += new System.EventHandler(this.salveazaLabirintToolStripMenuItem_Click);
            // 
            // incarcaLabirintToolStripMenuItem
            // 
            this.incarcaLabirintToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.incarcaLabirintToolStripMenuItem.Name = "incarcaLabirintToolStripMenuItem";
            this.incarcaLabirintToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.incarcaLabirintToolStripMenuItem.Text = "Incarca labirint";
            this.incarcaLabirintToolStripMenuItem.Click += new System.EventHandler(this.incarcaLabirintToolStripMenuItem_Click);
            // 
            // reseteazaJocToolStripMenuItem
            // 
            this.reseteazaJocToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.reseteazaJocToolStripMenuItem.Name = "reseteazaJocToolStripMenuItem";
            this.reseteazaJocToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.reseteazaJocToolStripMenuItem.Text = "Reseteaza joc";
            this.reseteazaJocToolStripMenuItem.Click += new System.EventHandler(this.reseteazaJocToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(161, 6);
            // 
            // iesireToolStripMenuItem
            // 
            this.iesireToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.iesireToolStripMenuItem.Name = "iesireToolStripMenuItem";
            this.iesireToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.iesireToolStripMenuItem.Text = "Iesire";
            this.iesireToolStripMenuItem.Click += new System.EventHandler(this.iesireToolStripMenuItem_Click);
            // 
            // ajutorToolStripMenuItem
            // 
            this.ajutorToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ajutorToolStripMenuItem.Enabled = false;
            this.ajutorToolStripMenuItem.Name = "ajutorToolStripMenuItem";
            this.ajutorToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.ajutorToolStripMenuItem.Text = "Ajutor";
            this.ajutorToolStripMenuItem.Click += new System.EventHandler(this.ajutorToolStripMenuItem_Click);
            // 
            // achivementsToolStripMenuItem
            // 
            this.achivementsToolStripMenuItem.Enabled = false;
            this.achivementsToolStripMenuItem.Name = "achivementsToolStripMenuItem";
            this.achivementsToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.achivementsToolStripMenuItem.Text = "Realizari";
            this.achivementsToolStripMenuItem.Click += new System.EventHandler(this.achivementsToolStripMenuItem_Click);
            // 
            // pictureBoxCos
            // 
            this.pictureBoxCos.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxCos.Location = new System.Drawing.Point(357, 399);
            this.pictureBoxCos.Name = "pictureBoxCos";
            this.pictureBoxCos.Size = new System.Drawing.Size(28, 28);
            this.pictureBoxCos.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxCos.TabIndex = 2;
            this.pictureBoxCos.TabStop = false;
            // 
            // pictureBoxChicken
            // 
            this.pictureBoxChicken.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxChicken.Location = new System.Drawing.Point(12, 399);
            this.pictureBoxChicken.Name = "pictureBoxChicken";
            this.pictureBoxChicken.Size = new System.Drawing.Size(28, 28);
            this.pictureBoxChicken.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxChicken.TabIndex = 1;
            this.pictureBoxChicken.TabStop = false;
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "lab";
            this.openFileDialog.FileName = "Incarca Labirint";
            this.openFileDialog.Title = "Incarca Labirint";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "lab";
            this.saveFileDialog.Title = "Salvare Labirint";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Location = new System.Drawing.Point(12, 55);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(389, 248);
            this.richTextBox1.TabIndex = 3;
            this.richTextBox1.Text = "";
            this.richTextBox1.Visible = false;
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(311, 454);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Timp ramas:";
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.Color.Lavender;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelTime,
            this.toolStripStatusViata,
            this.toolStripStatusScor,
            this.HighScoreStripStatusLabel,
            this.toolStripProgressBarLoading,
            this.baniStripStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 445);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(420, 22);
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabelTime
            // 
            this.toolStripStatusLabelTime.AutoSize = false;
            this.toolStripStatusLabelTime.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripStatusLabelTime.Name = "toolStripStatusLabelTime";
            this.toolStripStatusLabelTime.Size = new System.Drawing.Size(75, 17);
            this.toolStripStatusLabelTime.Text = "Timp: 40 sec";
            this.toolStripStatusLabelTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripStatusViata
            // 
            this.toolStripStatusViata.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripStatusViata.Name = "toolStripStatusViata";
            this.toolStripStatusViata.Size = new System.Drawing.Size(45, 17);
            this.toolStripStatusViata.Text = "Viata: 3";
            // 
            // toolStripStatusScor
            // 
            this.toolStripStatusScor.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripStatusScor.Name = "toolStripStatusScor";
            this.toolStripStatusScor.Size = new System.Drawing.Size(42, 17);
            this.toolStripStatusScor.Text = "Scor: 0";
            // 
            // HighScoreStripStatusLabel
            // 
            this.HighScoreStripStatusLabel.Name = "HighScoreStripStatusLabel";
            this.HighScoreStripStatusLabel.Size = new System.Drawing.Size(88, 17);
            this.HighScoreStripStatusLabel.Text = "High score: 0    ";
            // 
            // toolStripProgressBarLoading
            // 
            this.toolStripProgressBarLoading.Name = "toolStripProgressBarLoading";
            this.toolStripProgressBarLoading.Size = new System.Drawing.Size(100, 16);
            // 
            // baniStripStatusLabel
            // 
            this.baniStripStatusLabel.Name = "baniStripStatusLabel";
            this.baniStripStatusLabel.Size = new System.Drawing.Size(59, 15);
            this.baniStripStatusLabel.Text = "Bani: 0/20";
            this.baniStripStatusLabel.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 24);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(420, 443);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.ShowImageMargin = false;
            this.contextMenuStrip1.Size = new System.Drawing.Size(36, 4);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.Control;
            this.label2.Location = new System.Drawing.Point(354, 448);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Level 1";
            this.label2.Visible = false;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "i1.png");
            this.imageList1.Images.SetKeyName(1, "i2.png");
            this.imageList1.Images.SetKeyName(2, "i3.png");
            this.imageList1.Images.SetKeyName(3, "i4.png");
            this.imageList1.Images.SetKeyName(4, "i5.png");
            this.imageList1.Images.SetKeyName(5, "i6.png");
            this.imageList1.Images.SetKeyName(6, "i7.png");
            this.imageList1.Images.SetKeyName(7, "i8.png");
            this.imageList1.Images.SetKeyName(8, "i9.png");
            this.imageList1.Images.SetKeyName(9, "i10.png");
            this.imageList1.Images.SetKeyName(10, "i11.png");
            this.imageList1.Images.SetKeyName(11, "i12.png");
            // 
            // panel
            // 
            this.panel.BackColor = System.Drawing.Color.Peru;
            this.panel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel.BackgroundImage")));
            this.panel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel.Controls.Add(this.label3);
            this.panel.Controls.Add(this.label4);
            this.panel.Controls.Add(this.achievement10);
            this.panel.Controls.Add(this.achievement9);
            this.panel.Controls.Add(this.achievement8);
            this.panel.Controls.Add(this.achievement7);
            this.panel.Controls.Add(this.achievement6);
            this.panel.Controls.Add(this.achievement5);
            this.panel.Controls.Add(this.achievement4);
            this.panel.Controls.Add(this.achievement3);
            this.panel.Controls.Add(this.achievement2);
            this.panel.Controls.Add(this.achievement1);
            this.panel.Controls.Add(this.titluachievements);
            this.panel.Location = new System.Drawing.Point(95, 55);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(224, 192);
            this.panel.TabIndex = 10;
            this.panel.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(82, 137);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 19);
            this.label3.TabIndex = 13;
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 161);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 16);
            this.label4.TabIndex = 12;
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // achievement10
            // 
            this.achievement10.BackColor = System.Drawing.Color.Transparent;
            this.achievement10.Image = ((System.Drawing.Image)(resources.GetObject("achievement10.Image")));
            this.achievement10.Location = new System.Drawing.Point(173, 85);
            this.achievement10.Name = "achievement10";
            this.achievement10.Size = new System.Drawing.Size(33, 35);
            this.achievement10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.achievement10.TabIndex = 11;
            this.achievement10.TabStop = false;
            this.achievement10.Click += new System.EventHandler(this.achievement10_Click);
            // 
            // achievement9
            // 
            this.achievement9.BackColor = System.Drawing.Color.Transparent;
            this.achievement9.Image = ((System.Drawing.Image)(resources.GetObject("achievement9.Image")));
            this.achievement9.Location = new System.Drawing.Point(134, 85);
            this.achievement9.Name = "achievement9";
            this.achievement9.Size = new System.Drawing.Size(33, 35);
            this.achievement9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.achievement9.TabIndex = 10;
            this.achievement9.TabStop = false;
            this.achievement9.Click += new System.EventHandler(this.achievement9_Click);
            // 
            // achievement8
            // 
            this.achievement8.BackColor = System.Drawing.Color.Transparent;
            this.achievement8.Image = ((System.Drawing.Image)(resources.GetObject("achievement8.Image")));
            this.achievement8.Location = new System.Drawing.Point(95, 85);
            this.achievement8.Name = "achievement8";
            this.achievement8.Size = new System.Drawing.Size(33, 35);
            this.achievement8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.achievement8.TabIndex = 9;
            this.achievement8.TabStop = false;
            this.achievement8.Click += new System.EventHandler(this.achievement8_Click);
            // 
            // achievement7
            // 
            this.achievement7.BackColor = System.Drawing.Color.Transparent;
            this.achievement7.Image = ((System.Drawing.Image)(resources.GetObject("achievement7.Image")));
            this.achievement7.Location = new System.Drawing.Point(56, 85);
            this.achievement7.Name = "achievement7";
            this.achievement7.Size = new System.Drawing.Size(33, 35);
            this.achievement7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.achievement7.TabIndex = 8;
            this.achievement7.TabStop = false;
            this.achievement7.Click += new System.EventHandler(this.achievement7_Click);
            // 
            // achievement6
            // 
            this.achievement6.BackColor = System.Drawing.Color.Transparent;
            this.achievement6.Image = ((System.Drawing.Image)(resources.GetObject("achievement6.Image")));
            this.achievement6.Location = new System.Drawing.Point(17, 85);
            this.achievement6.Name = "achievement6";
            this.achievement6.Size = new System.Drawing.Size(33, 35);
            this.achievement6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.achievement6.TabIndex = 7;
            this.achievement6.TabStop = false;
            this.achievement6.Click += new System.EventHandler(this.achievement6_Click);
            // 
            // achievement5
            // 
            this.achievement5.BackColor = System.Drawing.Color.Transparent;
            this.achievement5.Image = ((System.Drawing.Image)(resources.GetObject("achievement5.Image")));
            this.achievement5.Location = new System.Drawing.Point(173, 44);
            this.achievement5.Name = "achievement5";
            this.achievement5.Size = new System.Drawing.Size(33, 35);
            this.achievement5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.achievement5.TabIndex = 6;
            this.achievement5.TabStop = false;
            this.achievement5.Click += new System.EventHandler(this.achievement5_Click);
            // 
            // achievement4
            // 
            this.achievement4.BackColor = System.Drawing.Color.Transparent;
            this.achievement4.Image = ((System.Drawing.Image)(resources.GetObject("achievement4.Image")));
            this.achievement4.Location = new System.Drawing.Point(134, 44);
            this.achievement4.Name = "achievement4";
            this.achievement4.Size = new System.Drawing.Size(33, 35);
            this.achievement4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.achievement4.TabIndex = 5;
            this.achievement4.TabStop = false;
            this.achievement4.Click += new System.EventHandler(this.achievement4_Click);
            // 
            // achievement3
            // 
            this.achievement3.BackColor = System.Drawing.Color.Transparent;
            this.achievement3.Image = ((System.Drawing.Image)(resources.GetObject("achievement3.Image")));
            this.achievement3.Location = new System.Drawing.Point(95, 44);
            this.achievement3.Name = "achievement3";
            this.achievement3.Size = new System.Drawing.Size(33, 35);
            this.achievement3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.achievement3.TabIndex = 4;
            this.achievement3.TabStop = false;
            this.achievement3.Click += new System.EventHandler(this.achievement3_Click);
            // 
            // achievement2
            // 
            this.achievement2.BackColor = System.Drawing.Color.Transparent;
            this.achievement2.Image = ((System.Drawing.Image)(resources.GetObject("achievement2.Image")));
            this.achievement2.Location = new System.Drawing.Point(56, 44);
            this.achievement2.Name = "achievement2";
            this.achievement2.Size = new System.Drawing.Size(33, 35);
            this.achievement2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.achievement2.TabIndex = 3;
            this.achievement2.TabStop = false;
            this.achievement2.Click += new System.EventHandler(this.achievements2_Click);
            // 
            // achievement1
            // 
            this.achievement1.BackColor = System.Drawing.Color.Transparent;
            this.achievement1.Image = ((System.Drawing.Image)(resources.GetObject("achievement1.Image")));
            this.achievement1.Location = new System.Drawing.Point(17, 44);
            this.achievement1.Name = "achievement1";
            this.achievement1.Size = new System.Drawing.Size(33, 35);
            this.achievement1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.achievement1.TabIndex = 2;
            this.achievement1.TabStop = false;
            this.achievement1.Click += new System.EventHandler(this.achievement1_Click);
            // 
            // titluachievements
            // 
            this.titluachievements.AutoSize = true;
            this.titluachievements.BackColor = System.Drawing.Color.Transparent;
            this.titluachievements.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titluachievements.Location = new System.Drawing.Point(23, 11);
            this.titluachievements.Name = "titluachievements";
            this.titluachievements.Size = new System.Drawing.Size(173, 19);
            this.titluachievements.TabIndex = 1;
            this.titluachievements.Text = "Realizari deblocate: 0/10";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(420, 467);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.pictureBoxCos);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBoxChicken);
            this.Controls.Add(this.menuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WallBreaker v2.4";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxChicken)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.achievement10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.achievement9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.achievement8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.achievement7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.achievement6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.achievement5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.achievement4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.achievement3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.achievement2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.achievement1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem jocToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jocNouToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iesireToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBoxChicken;
        private System.Windows.Forms.ToolStripMenuItem modificaLabirintToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem ajutorToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBoxCos;
        private System.Windows.Forms.ToolStripMenuItem salveazaLabirintToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem incarcaLabirintToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelTime;
        private System.Windows.Forms.ToolStripMenuItem micsoreazaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem maresteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusViata;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusScor;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBarLoading;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ToolStripStatusLabel HighScoreStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel baniStripStatusLabel;
        private System.Windows.Forms.ToolStripMenuItem achivementsToolStripMenuItem;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox achievement10;
        private System.Windows.Forms.PictureBox achievement9;
        private System.Windows.Forms.PictureBox achievement8;
        private System.Windows.Forms.PictureBox achievement7;
        private System.Windows.Forms.PictureBox achievement6;
        private System.Windows.Forms.PictureBox achievement5;
        private System.Windows.Forms.PictureBox achievement4;
        private System.Windows.Forms.PictureBox achievement3;
        private System.Windows.Forms.PictureBox achievement2;
        private System.Windows.Forms.PictureBox achievement1;
        private System.Windows.Forms.Label titluachievements;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripMenuItem reseteazaJocToolStripMenuItem;

    }
}

